import { productDetails } from "../../services/address";

// returns true if product available
export const isProductAvailable = async (product) => {
  const { childProductId, unitId: selectedUnitId } = product;
  const productResponse = await productDetails(childProductId);

  if (!productResponse || !productResponse.data) {
    return false;
  }

  const productData = productResponse.data.data;
  const { units = [] } = productData;
  const selectedUnitData = units.find((unit) => unit.unitId === selectedUnitId);

  // skip quantity check and return true, if user decreasing the qty.
  if (product.quantity <= selectedUnitData.availableQuantity) {
    return true;
  }
console.log(selectedUnitData,product.quantity,"product.quantity")
  return (
    selectedUnitData && selectedUnitData.availableQuantity > product.quantity
  );
};
