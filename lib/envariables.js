// deliv - x app variables

export const APP_NAME = "Leopards";

export const APP_DESC = "Leopards - One Store for all your need";

export const API_HOST = "https://api.leopards.de";
export const symbal = "₹";
// export const API_HOST = "https://api.deliv-x.com"

// export const MQTT_HOST = "wss://mqttdelivx.instacart-clone.com:2083/mqtt"

export const MQTT_HOST = "wss://mqttdelivx.loademup.xyz:2083/mqtt";

// export const FILTER_HOST = "https://apifilter.deliv-x.com"

export const FILTER_HOST = "https://apifilter.leopards.de";

export const OFFERS_HOST = "https://apifilter.leopards.de";

export const DESK_SMALL_LOGO = "/static/images/grocer/small_logo.png";

export const DESK_BIG_LOGO = "/static/images/grocer/icon.png";

export const DESK_LOGO = DESK_BIG_LOGO;

export const OG_LOGO =
  "https://website.deliv-x.com/static/images/updated/og_logo.png";

// export const DESK_LOGO = "/static/images/grocer/icon.png"

export const MOB_LOGO = "/static/images/grocer/icon.png";

export const SPLASH = "/static/images/grocer/icon.png";

// export const SPLASH = "/static/images/grocer/splash.gif"

export const FOOTER_TEXT = `© 2021 All rights reserved. XPECTRIGHT Technology Services`;

export const APP_STORE =
  "https://itunes.apple.com/us/app/delivx/id1450788084?mt=8";

export const PLAY_STORE =
  "https://play.google.com/store/apps/details?id=com.customer.delivX&hl=en";

export const STORE_APP_STORE =
  "https://itunes.apple.com/us/app/delivx-store/id1450264239";

export const STORE_PLAY_STORE =
  "https://play.google.com/store/apps/details?id=com.app.delivxstore";

export const WEB_LINK = "https://website.deliv-x.com";

export const BLOG_LINK =
  "https://blog.appscrip.com/page/2/?s=delivery&submit=Search";
// export const BLOG_LINK = "#";

// APP COLORS
export const BASE_COLOR = "#1A83C1";
export const WHITE_BASE_COLOR = "#fff";
export const BLUE_BASE_COLOR = "#2196f3";
export const OLD_SEMI_BASE_COLOR = "#1ec69f";

// APP ICONS
export const CART_ICON = <i className="fa fa-shopping-cart" />;
export const PROFILE_ICON = "/static/images/new_imgs/profileLogo/profile.svg";
export const ORDERS_ICON = "/static/images/new_imgs/profileLogo/orders.svg";
export const LOGOUT_ICON = "/static/images/new_imgs/profileLogo/logout.svg";
export const LOC_ICON_WHITE =
  "/static/images/new_imgs/map-marker-new-white.svg";
export const LOC_ICON_BLACK = "/static/images/new_imgs/map-marker-new.svg";
export const SEARCH_ICON = "/static/img/svg/searchDecent.svg";

export const OFFER_ICON_BLACK = "/static/images/new_imgs/OFFER_TAB.svg";
export const OFFER_ICON_GREEN = "/static/img/svg/discountMobi.svg";
export const EMPTY_TICKETS = "/static/images/new_imgs/support_ticket.png";

// FOOTER ICONS
export const SUPERMARKET_ICON = "/static/img/svg/Supermarket.svg";
export const RESTAURANT_ICON = "/static/img/svg/Supermarket.svg";

// HTML CONSTANTS
export const CART_PICKUP_BTN = "Self Pickup";
export const CART_DELIVERY_BTN = "Request Delivery";

export const CART_PICKUP = "Pickup";
export const CART_DELIVERY = "Delivery";

// HEAD TAG CONSTANTS - SEO

export const ORDER_FOOD = "Order Food Online from ";
export const ORDER_FOOD_SUBLINE =
  "and checkout the menu for Home Delivery. Fastest delivery | No minimum order | GPS tracking.";
export const OG_TITLE =
  "Leopards";
export const OG_DESC =
  "Leopards";

// EMPTY SCREENS

export const EMPTY_STORE = "/static/images/updated/empty/store.png";

// MATERIAL-UI CONTANTS
export const DRAWER_WIDTH_DESK = 480;

// PLACEHOLDER CONSTANTS
export const EXTRA_NOTES = "Add extra notes...";

// STORAGE CONSTANTS
export const NOTES_VAR = "dlvNotes";

//color
export const PRIMARY = "#f8ba1a";
