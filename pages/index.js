import React from "react";
import { Component } from "react";
import { connect } from "react-redux";

import * as enVariables from "../lib/envariables";
import * as actions from "../actions/index";
import Wrapper from "../hoc/wrapperHoc";
// import Footer from '../components/footer/Footer'
import LocationSelect from "../components/location/Locate";

import {
  setCookie,
  getCookie,
  getCookiees,
  removeCookie
} from "../lib/session";
import { redirectIfAuthenticated, getJwt, getLocation } from "../lib/auth";

import TopLoader from "../components/ui/loaders/TopBarLoader/TopBarLoader";

import "../assets/login.scss";
import "../assets/style.scss";
import CustomHead from "../components/html/head";


const Test = props => {
  console.log("inside test...", props);
};
class LocationPage extends Component {
  static async getInitialProps({ ctx }) {
    if (redirectIfAuthenticated(ctx)) {
      return {};
    }
    let token = await getCookiees("token", ctx.req);
    const userName = await getCookiees("username", ctx.req);
    const isAuthorized = await getCookiees("authorized", ctx.req);
    let lat = getCookiees("lat", ctx.req);
    let lng = getCookiees("long", ctx.req);
    let zoneID = getCookiees("zoneid", ctx.req);
    setCookie("lang", "en")
    // let lang = getCookiees("lang", ctx.req) || "en";
    // ctx && ctx.store ? ctx.store.dispatch(actions.selectLocale(lang)) : "";

    await ctx.store.dispatch(actions.checkCookies(ctx.req));

    return { userName, isAuthorized, token };
  }

  lang = this.props.lang;
  state = {
    showLogin: true,
    showSignup: false,
    username: this.props.userName,
    isAuthorized: this.props.isAuthorized,
    locationData: [],
    lat: "",
    long: "",
    loading: false,
    error: null,
    cityData: []
  };

  constructor(props) {
    super(props);
    this.getInnerRef = this.getInnerRef.bind(this);
    this.getLocation = this.getLocation.bind(this);
  }

  innerRef;

  getInnerRef(ref) {
    this.innerRef = ref;
  }

  getLocation() {
    this.innerRef && this.innerRef.getLocation();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.reduxState.userName != this.props.reduxState.userName) {
      console.log(
        "************************mapState*******************************",
        this.props.mapState
      );
      this.setState({
        username: this.props.reduxState.userName,
        isAuthorized: this.props.reduxState.authorized
      });
    }
  }

  componentDidMount() {
    let authorized = getCookie("authorized", "");
    let username = getCookie("username", "");
    setCookie("lang", "en")
    

    authorized
      ? (this.setState({ isAuthorized: true, username: username }),
        this.props.dispatch(actions.getProfile()))
      : this.props.dispatch(actions.guestLogin());
   
    this.setState({ isRendered: true });
}
  render() {
    const locale = this.props.locale;

    const { getInnerRef, getLocation } = this;

    return (
      <Wrapper>
       

        <CustomHead
          description={enVariables.OG_DESC}
          ogImage={enVariables.OG_LOGO}
          title={enVariables.OG_TITLE}
        />

        {/* <Demo setDetectedLocation={this.setDetectedLocation} ref={getInnerRef} />

                <button onClick={getLocation}>Get it</button> */}

        {this.state.isRendered ? (
          <LocationSelect
            loading={this.state.loading}
            isAuthorized={this.state.isAuthorized}
            locErrStatus={this.props.locErrStatus}
            locErrMessage={this.state.error}
            userName={this.state.username}
            showLoginHandler={this.showLoginHandler}
            showSignUpHandler={this.showSignUpHandler}
            updateLocation={this.updateLocation}
            getCategories={this.getCategories}
            getGeoLocation={this.getGeoLocation}
            goToProfile={this.goToProfile}
            cityData={this.state.cityData}
            lang={this.props.lang}
            locale={locale}
          />
        ) : (
          ""
        )}
       
        <TopLoader onRef={ref => (this.TopLoader = ref)} />
        
      </Wrapper>
    );
  }
}

const mapStateToProps = state => {
  return {
    reduxState: state,
    locErrStatus: state.locErrStatus,
    locErrMessage: state.locErrMessage,
    userProfileDetail: state.userProfile,
    selectedLang: state.selectedLang,
    locale: state.locale,
    lang: state.lang
  };
};

export default connect(mapStateToProps)(LocationPage);

// export default geolocated({
//     positionOptions: {
//         enableHighAccuracy: false,
//     },
//     userDecisionTimeout: 5000,
//     suppressLocationOnMount: true
// })(Demo);

// LocationPage.propTypes = { ...geoPropTypes };

// export default connect(mapStateToProps, null)(geolocated({ positionOptions: { enableHighAccuracy: true }, userDecisionTimeout: 5000, })(LocationPage));
