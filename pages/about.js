import React from "react";
import { connect } from "react-redux";

import Footer from "../components/footer/LandingFooter";
import MFooter from "../components/footer/Footer";

import { getCookiees } from "../lib/session";
import { appConfig } from "../services/auth";
import Wrapper from "../hoc/wrapperHoc";
import redirect from "../lib/redirect";
import CircularProgressLoader from "../components/ui/loaders/circularLoader";
import * as actions from "../actions";
import CustomHead from "../components/html/head";
import Hamburger from "../components/location/hamburger"
import {
  OG_LOGO,
  OG_TITLE,
  OG_DESC,
  APP_NAME,
  APP_STORE,
  PLAY_STORE
} from "../lib/envariables";
import BackButton from "../components/backbutton/back"

class About extends React.Component {
  lang = this.props.lang;
  static async getInitialProps({ ctx }) {
    const isAuthorized = await getCookiees("authorized", ctx.req);
    let StoreName = getCookiees("storeName", ctx.req);
    const queries = ctx.query;
    // let lang = (await (queries.lang || getCookiees("lang", ctx.req))) || "en";
    // ctx && ctx.store ? ctx.store.dispatch(actions.selectLocale(lang)) : "";
    return { isAuthorized, StoreName };
  }

  state = {
    aboutUs: null,
    loading: false
  };

  constructor(props) {
    super(props);

    this.state = {
      isAuthorized: props.isAuthorized
    };
  }

  showLoginHandler = () => {
    this.child.showLoginHandler();
  };
  showLocationHandler = () => {
    this.child.showLocationHandler();
  };
  showLocationMobileHandler = () => {
    this.child.showLocationMobileHandler();
  };
  showCart = () => {
    this.child.showCart();
  };
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.reduxState.userName !== this.props.reduxState.userName) {
      this.setState({
        username: this.props.reduxState.userName,
        isAuthorized: this.props.reduxState.authorized
      });
    }
  }
  componentDidMount = () => {
    this.setState({ loading: true });
    appConfig()
      .then(async ({ data }) => {
        console.log(data);
        await this.setState({ aboutUs: data.data.aboutUs, loading: false });

        let htmlTag = document.getElementById("whtDesc");
        htmlTag.innerHTML = data.data.aboutUs.question[0].description;

        htmlTag = document.getElementById("whyDesc");
        htmlTag.innerHTML = data.data.aboutUs.question[1].description;
      })
      .catch(err => this.setState({ loading: false }));
  };
  deleteAllCookies() {
    document.cookie.split(";").forEach(c => {
      document.cookie =
        c.trim().split("=")[0] +
        "=;" +
        "expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    });
    redirect("/");
  }
  render() {
    let lang = this.props.lang;
    return (
      <div>
        <CustomHead
          description={"About DelivX App, I can sya it's all in one" || OG_DESC}
          ogImage={OG_LOGO}
          title={OG_TITLE}
        />

     
<div className="mobile-show">
<Hamburger/>
</div>
<div className="container">
<BackButton name ={"Impressum"}/>
  </div>       
        {this.state.aboutUs ? (
          <Wrapper>
            <div className="container aboutSecLayout">
              <h4 className="aboutSecH2">
                {this.state.aboutUs &&
                  this.state.aboutUs.question &&
                  this.state.aboutUs.question[0].title}
              </h4>
          
              <p className="aboutSecP" id="whtDesc"></p>
            </div>
            
          </Wrapper>
        ) : (
          ""
        )}

        {/* Mobile Icons Container */}
        {/* <div className="container p-3 p-sm-5" id="mobiIconsContainer"> */}
        <div className="container py-5 d-none" id="mobiIconsContainer">
          
          <div className="row">
            <div className="col-12 text-center">
              <h4 className="mainAreaTitle">
                {APP_NAME}{" "}
                {this.lang.aboutText ||
                  "is the place for you to discover and buy amazing things curated by our global community"}
                .
              </h4>
            </div>
          </div>
        
          <div className="row">
            <div className="col-md-6">
              <img
                src="../static/images/about-mobile-icons.png"
                className="img-fluid"
              />
            </div>
            <div className="col-md-6 py-md-5 aboutUsSocialIconsSecBt align-self-center">
              <div className="row justify-content-center">
                <div className="col-md-10">
                  <h3 className="">
                    {lang.webMobile || "Web, mobile and beyond"}
                  </h3>
                  <p className="aboutUsSocialIconPTag">
                    {APP_NAME}{" "}
                    {lang.avalable || "is available on iOS, Android & Web."}
                  </p>
                  <p className="aboutUsSocialIconPTag">
                    {lang.downloadThe || "Download the"} {APP_NAME}{" "}
                    {lang.app || "app"}
                  </p>
                  <div className="row mt-3">
                   
                    <div className="col-xl-4 col-lg-5 col-md-6 col-4">
                      <a href={PLAY_STORE} target="_blank">
                        <img
                          alt="App store"
                          src="https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/playStore.svg"
                          width=""
                          height=""
                          className=""
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {this.state.aboutUs &&
        this.state.aboutUs.visinor &&
        this.state.aboutUs.visinor.length > 0 ? (
          <div className="container">
            <h4 className="aboutSecH2">
              {lang.meetTheTeam || "Meet the team"}
            </h4>
            {
              <div className="row">
                {this.state.aboutUs.visinor &&
                  this.state.aboutUs.visinor.map((vsnData, index) => (
                    <div
                      className="col-lg-4 col-md-6 mb-3 mb-md-0"
                      key={"member" + index}
                      style={{ display: "flex", alignItems: "stretch" }}
                    >
                      <div className="meetTeamSection px-4">
                        <div className="row mb-3">
                          <div className="col-auto meetTeamImgLayout">
                            <img
                              className=""
                              src={vsnData.visinorImages}
                              width="100"
                              height="100"
                              alt={vsnData.visinorName}
                            />
                          </div>
                          <div className="col">
                            <h4 className="meetTeamH4">
                              {vsnData.visinorName}
                            </h4>
                            <p className="meetTeamP">
                              {vsnData.visinorDesignation}
                            </p>
                            <div className="meetTeamSocialLinksLayout">
                              {vsnData.visinorFacebook &&
                              vsnData.visinorFacebook.length > 0 ? (
                                <a
                                  className="socialLink"
                                  href={vsnData.visinorFacebook}
                                >
                                  <img
                                    className="socialLinkImg"
                                    src="https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/Ffacebook.svg"
                                    width="24"
                                    height="24"
                                  />
                                </a>
                              ) : (
                                ""
                              )}
                              {vsnData.visinorLinkedin &&
                              vsnData.visinorLinkedin.length > 0 ? (
                                <a
                                  className="socialLink"
                                  href={vsnData.visinorLinkedin}
                                >
                                  <img
                                    className="socialLinkImg"
                                    src="https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/Flinkedin.svg"
                                    width="24"
                                    height="24"
                                  />
                                </a>
                              ) : (
                                ""
                              )}
                              {vsnData.visinorTwitter &&
                              vsnData.visinorTwitter.length > 0 ? (
                                <a
                                  className="socialLink"
                                  href={vsnData.visinorTwitter}
                                >
                                  <img
                                    className="socialLinkImg"
                                    src="https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/Ftwitter.svg"
                                    width="24"
                                    height="24"
                                  />
                                </a>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col">
                            <p className="meetTeamDesc">
                              {vsnData.visinorDescription}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            }
          </div>
        ) : (
          ""
        )}
        <div className=" mt-5" className="footerContainer mobile-hide">
          <Footer lang={this.props.lang} />
        </div>
        <div className="" className=" mobile-show ">
          <MFooter lang={this.props.lang} />
        </div>
        {this.state.loading ? <CircularProgressLoader /> : ""}
        {/* <Authmodals
          onRef={ref => (this.child = ref)}
          editCart={this.editCart}
        /> */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    reduxState: state,
    selectedLang: state.selectedLang,
    lang: state.locale
  };
};

export default connect(mapStateToProps)(About);
