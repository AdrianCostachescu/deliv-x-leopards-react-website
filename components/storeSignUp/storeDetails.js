import Wrapper from "../../hoc/wrapperHoc";
import AuthInput from "../ui/auth/input";
import Intelinput from "../ui/intelinput";
import LinearProgressBar from "../ui/linearProgress";
import LocationSearchInput from "../location/map";
import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input";
import { parsePhoneNumberFromString } from "libphonenumber-js";
import "../../assets/storeList.scss";
import Swiches from "../../components/swich/swich";
let isPhoneValid = false;
import React, { useState, useEffect } from "react";
import { symbal, WHITE_BASE_COLOR } from "../../lib/envariables";
import RadioButton from "./radio-button";
import CheckBoxButton from "./checkbox-button";
const StoreDetailInPuts = (props) => {
  let initialState = {
    countryCode: "",
    phoneNo: "",
    valid: true,
  };
  const [state, setstate] = useState(initialState);

  const [BusinessPhoneNo, setNumber] = useState({
    countryCode: "",
    phoneNo: "",
    valid: true,
  });
  const [focus, setFocus] = useState({});
  const [inputValue, setInputValue] = useState({});
// update login phone
  const updateLoginPhone = (phone) => {
    const phoneNumber = phone
      ? parsePhoneNumberFromString(phone.toString())
      : "";
    const countryIntl = phoneNumber ? phoneNumber.country : "IN";
    const re = /[0-9]+/g;
   
    let valid = isValidPhoneNumber(phone);
    // if ((e.target.value === '' || re.test(e.target.value)) && e.target.value.length <= 10) {
    setstate({
      phoneNo: phone,
      cc: phoneNumber ? "+" + phoneNumber.countryCallingCode : "+91",
      lgPhone: phoneNumber ? phoneNumber.nationalNumber : "",
      countryIntl: countryIntl,
      valid: valid,
    });
    let e = {
      target: {
        value: phoneNumber ? "+" + phoneNumber.countryCallingCode : "+91",
      },
    };
    props.updateForm(e, "countryCode");
    e = {
      target: {
        value: phoneNumber ? phoneNumber.nationalNumber : "",
      },
    };

    props.updateForm(e, "ownerPhone");
    let inputObject = { ...inputValue };

    inputObject["ownerPhone"] = {
      phoneNo: phoneNumber ? "+" + phoneNumber.countryCallingCode : "+91",
      countryIntl: phoneNumber ? phoneNumber.nationalNumber : "",
    };
    setInputValue(inputObject);
    // ownerPhone
    // countryId
    // }
  };

  const updateLoginPhone1 = (phone) => {
    const phoneNumber = phone
      ? parsePhoneNumberFromString(phone.toString())
      : "";
    const countryIntl = phoneNumber ? phoneNumber.country : "IN";
    const re = /[0-9]+/g;
   
    // if ((e.target.value === '' || re.test(e.target.value)) && e.target.value.length <= 10) {
    setNumber({
      phoneNo: phone,
      cc: phoneNumber ? "+" + phoneNumber.countryCallingCode : "+91",
      lgPhone: phoneNumber ? phoneNumber.nationalNumber : "",
      countryIntl: countryIntl,
      valid: isValidPhoneNumber(phone),
    });
    let e = {
      target: {
        value: phoneNumber ? "+" + phoneNumber.countryCallingCode : "+91",
      },
    };
    // props.updateForm(e, "countryCode");
    e = {
      target: {
        value: phoneNumber ? phoneNumber.nationalNumber : "",
      },
    };

    setInputValue(inputObject);
    let inputObject = { ...inputValue };
    inputObject["BusinessPhoneNo"] = {
      countryIntl: phoneNumber ? "+" + phoneNumber.countryCallingCode : "+91",
      phoneNo: phoneNumber ? phoneNumber.nationalNumber : "",
    };
    setInputValue(inputObject);
    // props.updateForm(e, "ownerPhone");

    // ownerPhone
    // countryId
    // }
  };
// handle address
  const handlerAddress = (data) => {
    let focusObject = { ...focus };
    focusObject["address"] = true;
    let inputObject = { ...inputValue };
    inputObject["address"] = data.address;
    setFocus(focusObject);
    setInputValue(inputObject);
    props.updateLocation(data);
    setTimeout(() => {
     
    }, 1000);
  };

  const onInputHandler = (e) => {
    let focusObject = { ...focus };
    focusObject[e.target.name] = true;
    let inputObject = { ...inputValue };
    inputObject[e.target.name] = e.target.value;
    setFocus(focusObject);
    setInputValue(inputObject);

    setTimeout(() => {
     
    }, 1000);
  };
  function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
      return true;
    } else if (key < 48 || key > 57) {
      return false;
    } else {
      return true;
    }
  }

  useEffect(() => {
    $("[type=number]").keypress(validateNumber);
  }, []);
// handle file select
  const onFileSelect = (e) => {
   

    let focusObject = { ...focus };
    focusObject[e.target.name] = true;
    let inputObject = { ...inputValue };
    inputObject[e.target.name] = e.target.files[0];
    setFocus(focusObject);
    setInputValue(inputObject);

    setTimeout(() => {
   
    }, 1000);
  };

  const onCheckBoxHanlder = (e) => {
    

    let target = e.target;
    let check = false;
    if (target.checked) {
      target.removeAttribute("checked");
      target.parentNode.style.textDecoration = "";
      check = true;
    } else {
      target.setAttribute("checked", true);
      target.parentNode.style.textDecoration = "line-through";
      check = false;
    }

    let focusObject = { ...focus };
    focusObject[e.target.name] = true;
    let inputObject = { ...inputValue };

    inputObject[e.target.name] = check;
    setFocus(focusObject);
    setInputValue(inputObject);
  };
console.log(inputValue,"inputValue")
  return (
    <Wrapper>
      <div className="col store-details">
        <div className="row">
          <div className="col-12 mt-3">
            <div className="row">
              <div
                className="col-12 d-flex align-items-center"
                style={{ height: "60px" }}
              >
                <h5 className="registerStoreFormH5Title">Persönliche Informationen</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <label className="mr-2">
                Bewerbungsfoto
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  {/* <AuthInput
                    type="text"
                    label="Name*"
                    name="storename"
                    id="storename"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.storeName}
                    dynamicValue={"storeName"}
                  /> */}
                  <div className="form-group mt-3">
                    <input
                      onChange={onFileSelect}
                      type="file"
                      name="profileImage"
                    />
                  </div>
                </div>
              </div>
              <div className="col-12 d-none">
                <label className="mr-2 mt-3">
                BANNERBILDER
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  {/* <AuthInput
                    type="text"
                    label="Name*"
                    name="storename"
                    id="storename"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.storeName}
                    dynamicValue={"storeName"}
                  /> */}
                  <div className="form-group mt-3">
                    <input
                      onChange={onFileSelect}
                      type="file"
                      name="bannerImage"
                    />
                  </div>
                </div>
              </div>
              <div className="col-12">
                <label className="mr-2 mt-3">
                Vorname
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  {/* <AuthInput
                    type="text"
                    label="Name*"
                    name="storename"
                    id="storename"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.storeName}
                    dynamicValue={"storeName"}
                  /> */}
                  <div className="form-group mt-3">
                    <input
                      mendatory="true"
                      onChange={onInputHandler.bind(true)}
                      type="text"
                      name="storeName"
                      className="input_container"
                      placeholder="Vorname eingeben"
                    />
                  </div>
                </div>
                {!inputValue.storeName && focus.storeName && (
                  <div className="text-danger">Shopname erforderlich!</div>
                )}
              </div>
              <div className="col-12">
                <label className="mr-2 mt-3">
                Nachname
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  {/* <AuthInput
                    type="text"
                    label="Name*"
                    name="storename"
                    id="storename"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.storeName}
                    dynamicValue={"storeName"}
                  /> */}
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="text"
                      name="ownerName"
                      className="input_container"
                      placeholder="Nachname eingeben"
                    />
                  </div>
                </div>
                {!inputValue.ownerName && focus.ownerName && (
                  <div className="text-danger">Besitzername erforderlich!</div>
                )}
              </div>

              <div className="col-xl-6">
                <label className="mr-2 mt-3">Telefonnummer</label>
                <div className="outline-box p-0 ownerNumber">
                  {/* <div className="col-xl-3 col-3 p-0 float-left mobInputWidth"> */}
                  {/* <Intelinput id="login-intel-input" /> */}
                  {/* </div> */}
                  <div className="col-xl-9 col-9 pl-3 pl-sm-0 pl-xl-3 float-left">
                    <PhoneInput
                      countrySelectProps={{ disabled: true }}
                      country="DE"
                      //country={"IN"}
                      placeholder="Telefonnummer eingeben"
                      value={state.phoneNo}
                      onChange={updateLoginPhone}
                    />
                    {/* <AuthInput
                      type="text"
                      label="Phone*"
                      id="OwnerNumber"
                      name="Ownerphone"
                      dynamic={true}
                      onChange={props.updateForm}
                      value={props.state.ownerNumber}
                      allowNumber={props.allowOnlyNumber}
                      dynamicValue={"ownerNumber"} */}
                    {/* /> */}
                  </div>
                </div>

                {inputValue.ownerPhone &&
                  state.phoneNo != "" &&
                  !state.valid && (
                    <div className="text-danger">Geben Sie eine gültige Telefonnummer ein!</div>
                  )}
              </div>
              <div className="col-12">
                <label className="mr-2 mt-3">
                E-Mail Adresse

                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  {/* <AuthInput
                    type="text"
                    label="Name*"
                    name="storename"
                    id="storename"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.storeName}
                    dynamicValue={"storeName"}
                  /> */}
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="text"
                      name="ownerEmail"
                      className="input_container"
                      placeholder="Bitte E-Mail Adresse eingeben"
                    />
                  </div>
                </div>
                {!inputValue.ownerEmail && focus.ownerEmail && (
                  <div className="text-danger">E-Mail des Besitzers erforderlich!</div>
                )}
              </div>
              <div className="col-12">
                <label className="mr-2 mt-3">
                Kennwort
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  {/* <AuthInput
                    type="text"
                    label="Name*"
                    name="storename"
                    id="storename"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.storeName}
                    dynamicValue={"storeName"}
                  /> */}
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="password"
                      name="password"
                      className="input_container"
                      placeholder="Bitte Kennwort eingeben"
                    />
                  </div>
                </div>
                {!inputValue.password && focus.password && (
                  <div className="text-danger">Passwort erforderlich !
                  </div>
                )}
              </div>
              <div className="col-xl-6 d-none">
                <label className="mr-2 mt-3">
                GESCHÄFTSTELEFON

                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box p-0 ownerNumber">
                  {/* <div className="col-xl-3 col-3 p-0 float-left mobInputWidth"> */}
                  {/* <Intelinput id="login-intel-input" /> */}
                  {/* </div> */}
                  <div className="col-xl-9 col-9 pl-3 pl-sm-0 pl-xl-3 float-left">
                    <PhoneInput
                      countrySelectProps={{ disabled: true }}
                      country="IN"
                      //country={"IN"}
                      placeholder="Telefonnummer eingeben"
                      value={BusinessPhoneNo.phoneNo}
                      onChange={updateLoginPhone1}
                    />
                    {/* <AuthInput
                      type="text"
                      label="Phone*"
                      id="OwnerNumber"
                      name="Ownerphone"
                      dynamic={true}
                      onChange={props.updateForm}
                      value={props.state.ownerNumber}
                      allowNumber={props.allowOnlyNumber}
                      dynamicValue={"ownerNumber"} */}
                    {/* /> */}
                  </div>
                </div>
                {inputValue.BusinessPhoneNo && !BusinessPhoneNo.valid && (
                  <div className="text-danger">Geben Sie eine gültige Telefonnummer ein!</div>
                )}
              </div>
              <div className="col-12 d-none">
                <label className="mr-2 mt-3">WEBSEITE</label>
                <div className="outline-box" id="businessEmail">
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="text"
                      name="website"
                      className="input_container"
                      placeholder="WEBSEITE"
                    />
                  </div>
                  {/* <span className="error" style={{right:'-122px', display:'none'}} id="businessEmailError">You have entered invalid URL. Please try again.</span> */}
                </div>
              </div>
              <div className="col-12 d-none">
                <label className="mr-2 mt-3">BESCHREIBUNG</label>
                <div className="outline-box">
                  {/* <AuthInput
                    type="text"
                    label="Name*"
                    name="storename"
                    id="storename"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.storeName}
                    dynamicValue={"storeName"}
                  /> */}
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="text"
                      name="storedescription"
                      className="input_container"
                      placeholder="Beschreibung"
                    />
                  </div>
                </div>
              </div>
              {/* <div className="col-12"> */}
              {/* <label className="mr-2 mt-2">CATEGORY<span style={{color:"red"}}> <sup>*</sup> </span></label> */}

              {/* <div class="form-group"> */}
              {/* <select class="form-control" id="exampleFormControlSelect1" name="platform" required="required">
              <option>CATEGORY</option>
              <option>Home supplies</option>

            </select> */}
              {/* </div> */}

              {/* </div> */}
              {/* <div className="col-12">
              <label className="mr-2 mt-2">SUB CATEGORY<span style={{color:"red"}}> <sup>*</sup> </span></label>
               
                <div class="form-group">
            <select class="form-control" id="exampleFormControlSelect1" name="platform" required="required">
              <option>SUB CATEGORY</option>
              <option>Home supplies</option>

            </select>
          </div>
                
              </div> */}
              {/* <div className="col-12">
                <label className="mr-2 mt-2">
                  COUNTRY
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>

                <div class="form-group">
                  <select
                    onChange={onInputHandler}
                    class="form-control"
                    id="exampleFormControlSelect1"
                    name="country"
                    required="required"
                  >
                    <option value="india">india</option>
                  </select>
                </div>
              </div> */}
              {/* <div className="col-12">
                <label className="mr-2 mt-2">
                  CITY
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>

                <div class="form-group">
                  <select
                    class="form-control"
                    id="exampleFormControlSelect1"
                    name="platform"
                    required="required"
                  >
                    <option>Select City</option>
                    <option>keshod</option>
                  </select>
                </div>
              </div> */}
              <div className="col-12 ">
                <label className="mr-2 mt-3">
                RECHNUNGSADRESSE
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>

                <div className="form-group mt-3">
                  <LocationSearchInput
                    placeHolder="Adresse*"
                    updateLocation={handlerAddress}
                    locale={props.locale}
                    inputClassName="form-control py-lg-3 rounded-0 selectAddress overflow-ellipsis"
                    // ref={ref => this.LocSearchRef = ref}
                  />
                </div>
                {!inputValue.address && focus.address && (
                  <div className="text-danger">Adressname erforderlich !</div>
                )}
                {/* {!inputValue.address && focus.ownerName && (
                    <div className="text-danger">Owner email required !</div>
                  )} */}
              </div>
              <div className="col-12 d-none">
                <label className="mr-2 mt-3"></label>
                <div className="form-group mt-3">
                  <LocationSearchInput
                    placeHolder="Adresse*"
                    updateLocation={props.updateLocation1}
                    locale={props.locale}
                    inputClassName="form-control py-lg-3 rounded-0 selectAddress overflow-ellipsis"
                    // ref={ref => this.LocSearchRef = ref}
                  />
                </div>
              </div>
              <div className="col-12">
                <label className="mr-2 mt-3">
                Straße und Hausnummer
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="text"
                      name="streetName"
                      className="input_container"
                    />
                  </div>
                </div>
                {!inputValue.streetName && focus.streetName && (
                  <div className="text-danger">Straßenname erforderlich!</div>
                )}
              </div>
              <div className="col-12">
                <label className="mr-2 mt-3">
                Postleitzahl
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="text"
                      name="localityName"
                      className="input_container"
                    />
                  </div>
                </div>
                {!inputValue.localityName && focus.localityName && (
                  <div className="text-danger">Ortsname erforderlich !</div>
                )}
              </div>
              <div className="col-12">
                <label className="mr-2 mt-3">
                Ort
                  <span style={{ color: "#1A83C1" }}>
                    {" "}
                    <sup>*</sup>{" "}
                  </span>
                </label>
                <div className="outline-box">
                  <div className="form-group mt-3">
                    <input
                      onChange={onInputHandler}
                      type="text"
                      name="areaName"
                      className="input_container"
                    />
                  </div>
                </div>
                {!inputValue.areaName && focus.areaName && (
                  <div className="text-danger">Gebietsname erforderlich !</div>
                )}
              </div>
              {/* <div className="col-12"> */}
              {/* <label className="mr-2 mt-3">SERVICE ZONES</label> */}
              {/* <div className="form-group mt-3">
                  <textarea
                    id="w3mission"
                    rows="3"
                    cols="50"
                    placeholder="Business Address"
                  ></textarea>
                </div>
              </div> */}
              {/* <div className="swicth_button col-12 mt-3">
                <div className="col-4 p-0">
                  <p style={{ margin: "0" }}>AUTOMATISCHE GENEHMIGUNG</p>
                </div>
                <div className="col-4">
                  <Swiches name="autoApproval" onChange={onInputHandler} />
                </div>
              </div> */}
            </div>
          </div>

          <div className="col-12 mt-3">
            <div className="row">
              <div
                className="col-12 d-flex align-items-center"
                style={{ height: "60px" }}
              >
                {/* <h5 className="registerStoreFormH5Title">TREIBEREINSTELLUNGEN</h5> */}
              </div>
            </div>
            <div className="row">
              {/* <div className="col-xl-6" style={{ paddingRight: '5px' }}> */}
              <div className="col-12 ">
                <div className="col-12 p-0 d-flex">
                  <div className="col-xl-4 col-sm-6 p-0">
                    <p style={{ margin: "0" }}>
                    Beschäftigung
                      <span style={{ color: "#1A83C1" }}>
                        {" "}
                        <sup>*</sup>{" "}
                      </span>
                    </p>
                  </div>
                  <div className="col-xl-8 col-sm-6 pl-3 row m-0">
                    <RadioButton
                      type="radio"
                      id="driverType"
                      onChange={onInputHandler}
                      name="driverType"
                      value="2"
                      label="Vollzeit"
                    ></RadioButton>
                    <RadioButton
                      type="radio"
                      id="driverType1"
                      onChange={onInputHandler}
                      name="driverType"
                      value="1"
                      label="Teilzeit"
                    ></RadioButton>
                     <RadioButton
                      type="radio"
                      id="driverType2"
                      onChange={onInputHandler}
                      name="driverType"
                      value="0"
                      label="Minijob"
                    ></RadioButton>
                  </div>
                </div>
              </div>
              <div className="swicth_button col-12 pt-2">
                <div className="col-xl-4 col-sm-6 p-0">
                  <p style={{ margin: "0" }}>Ich bin älter als 18 Jahre  <span style={{ color: "#1A83C1" }}>
                        {" "}
                        <sup>*</sup>{" "}
                      </span>
</p>
                </div>
                <div className="col-xl-4 col-sm-6">
                  <Swiches name="forcedAccept" onChange={onInputHandler} />
                </div>
              </div>
              <div className="swicth_button col-12 pt-2">
                <div className="col-xl-4 col-sm-6 p-0">
                  <p style={{ margin: "0" }}>Ich darf in Deutschland arbeiten  <span style={{ color: "#1A83C1" }}>
                        {" "}
                        <sup>*</sup>{" "}
                      </span></p>
                </div>
                <div className="col-xl-4 col-sm-6">
                  <Swiches name="autoDispatch" onChange={onInputHandler} />
                </div>
              </div>
              <div className="swicth_button col-12 pt-2">
                <div className="col-xl-4 col-sm-6 p-0">
                  <p style={{ margin: "0" }}>Ich stimme den Leopards Datenschutzrichtlinien zu  <span style={{ color: "#1A83C1" }}>
                        {" "}
                        <sup>*</sup>{" "}
                      </span></p>
                </div>
                <div className="col-xl-4 col-sm-6">
                  <Swiches name="accsept" onChange={onInputHandler} />
                </div>
              </div>
              {/* <div className="col-xl-6">
                <div className="outline-box">
                  <AuthInput
                    type="text"
                    label="Name*"
                    name="ownername"
                    id="ownername"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.ownerName}
                    dynamicValue={"ownerName"}
                  />
                </div>
              </div> */}
              {/* <div className="col-xl-6" style={{ paddingLeft: '5px' }}> */}

              {/* <div className="col-xl-6" style={{ paddingRight: '5px' }}> */}
              {/* <div className="col-xl-6">
                <div className="outline-box" id="ownerEmail">
                  <AuthInput
                    type="text"
                    label="Email*"
                    name="owneremail"
                    id="owneremail"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.ownerEmail}
                    dynamicValue={"ownerEmail"}
                    focusOut={props.validateEmail}
                    focusIn={props.focusedIn}
                  />
                  <span
                    className="error"
                    style={{ left: "-122px", top: "0px", display: "none" }}
                    id="ownerEmailError"
                  >
                    You have entered invalid E-mail. Please try again.
                  </span>
                </div>
              </div> */}
              {/* <div className="col-xl-6" style={{ paddingLeft: '5px' }}> */}
              {/* <div className="col-xl-6">
                <div className="outline-box">
                  <AuthInput
                    type="password"
                    label="Password*"
                    name="ownerpassword"
                    id="ownerpassword"
                    dynamic={true}
                    onChange={props.updateForm}
                    value={props.state.ownerPassword}
                    dynamicValue={"ownerPassword"}
                  />
                </div>
              </div> */}
            </div>
          </div>
          <div className="col-12 mt-3 d-none">
            <div className="row">
              <div
                className="col-12 d-flex align-items-center"
                style={{ height: "60px" }}
              >
                <h5 className="registerStoreFormH5Title">PREISEINSTELLUNGEN</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-12 ">
                <div className="col-12 p-0 d-flex align-items-center">
                  <div className="col-xl-4 col-sm-6 p-0">
                    <p style={{ margin: "0" }}>
                    PREISMODELL
                      <span style={{ color: "#1A83C1" }}>
                        {" "}
                        <sup>*</sup>{" "}
                      </span>
                    </p>
                  </div>
                  <div className="col-xl-8 col-sm-6 pl-3 row m-0">
                  
                    <RadioButton
                      type="radio"
                      id="pricingMode2"
                      onChange={onInputHandler}
                      name="pricingModel"
                      value="1"
                      label="  Kilometerpreise"
                    ></RadioButton>
                  </div>
                </div>
              </div>
              <div className="col-12 d-flex align-items-center mt-3">
                <div className="col-xl-4 col-sm-6 p-0">
                  <p style={{ margin: "0" }}>
                  MINDESTBESTELLWERT({symbal})
                    <span style={{ color: "#1A83C1" }}>
                      {" "}
                      <sup>*</sup>{" "}
                    </span>
                  </p>
                </div>
                <div className="col-xl-8 col-sm-6 pl-3 d-flex align-items-center">
                  <input
                    type="number"
                    onChange={onInputHandler}
                    name="minOrderValue"
                    placeholder="Minimum Order Value"
                    className="input_box"
                  />
                  <div className="corrency">{symbal}</div>
                  {!inputValue.minOrderValue && focus.minOrderValue && (
                    <div className="text-danger">Mindestbestellmenge erforderlich!</div>
                  )}
                </div>
              </div>
              <div className="col-12 d-flex align-items-center mt-3">
                <div className="col-xl-4 col-sm-6 p-0">
                  <p style={{ margin: "0" }}>
                  SCHWELLWERT ({symbal})
                    <span style={{ color: "#1A83C1" }}>
                      {" "}
                      <sup>*</sup>{" "}
                    </span>
                  </p>
                </div>
                <div className="col-xl-8 col-sm-6 pl-3 d-flex align-items-center">
                  <input
                    type="number"
                    name="freeDeliveryAbove"
                    onChange={onInputHandler}
                    placeholder="Schwellwert"
                    className="input_box"
                  />
                  <div className="corrency">{symbal}</div>
                  {!inputValue.thresoldValue && focus.thresoldValue && (
                    <div className="text-danger">Bestellschein erforderlich !</div>
                  )}
                </div>
              </div>
              <div className="col-12 d-flex align-items-center mt-3">
                <div className="col-xl-4 col-sm-6 p-0">
                  <p style={{ margin: "0" }}>DURCHSCHNITTLICHE LIEFERZEIT</p>
                </div>
                <div className="col-xl-8 col-sm-6 pl-3 d-flex">
                  <input
                    type="number"
                    onChange={onInputHandler}
                    name="avgDeliveryTime"
                    placeholder="Durchschnittliche Lieferzeit"
                    className="input_box"
                  />
                  <div className="minutes">Protokoll</div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 mt-3 d-none">
            <div className="row">
              <div
                className="col-12 d-flex align-items-center"
                style={{ height: "60px" }}
              >
                <h5 className="registerStoreFormH5Title">SERVICEEINSTELLUNGEN</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-12 d-flex align-items-center mt-3">
                <div className="col-xl-4 col-sm-6 p-0">
                  <p style={{ margin: "0" }}>
                  KOSTEN FÜR ZWEI
                    <span style={{ color: "#1A83C1" }}>
                      {" "}
                      <sup>*</sup>{" "}
                    </span>
                  </p>
                </div>
                <div className="col-xl-8 col-sm-6 pl-3 d-flex align-items-center">
                  <input
                    type="number"
                    name="costForTwo"
                    onChange={onInputHandler}
                    placeholder="Kosten für zwei"
                    className="input_box"
                  />
                  <div className="corrency">{symbal}</div>
                  {!inputValue.costForTwo && focus.costForTwo && (
                    <div className="text-danger">Kosten für zwei erforderlich!</div>
                  )}
                </div>
              </div>
              <div className="col-12 mt-3">
                <div className="col-12 p-0 d-flex align-items-center">
                  <div className="col-xl-4 col-sm-6 p-0">
                    <p style={{ margin: "0" }}>
                    AUFTRAGSART
                      <span style={{ color: "#1A83C1" }}>
                        {" "}
                        <sup>*</sup>{" "}
                      </span>
                    </p>
                  </div>
                  <div className="col-xl-8 col-sm-6 pl-3 row m-0">
                    <RadioButton
                      type="radio"
                      id="orderType1"
                      onChange={onInputHandler}
                      name="orderType"
                      value="1"
                      label="Abholen"
                    ></RadioButton>
                    <RadioButton
                      type="radio"
                      id="orderType2"
                      onChange={onInputHandler}
                      name="orderType"
                      value="2"
                      label="Lieferung"
                    ></RadioButton>
                    <RadioButton
                      type="radio"
                      onChange={onInputHandler}
                      id="orderType3"
                      name="orderType"
                      value="3"
                      label="Beide"
                    ></RadioButton>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 mt-3 d-none">
            <div className="row d-none">
              <div
                className="col-12 d-flex align-items-center"
                style={{ height: "60px" }}
              >
                <h5 className="registerStoreFormH5Title">ZAHLUNGSEINSTELLUNGEN</h5>
              </div>
            </div>
            {(!inputValue.orderType ||
              inputValue.orderType == "1" ||
              inputValue.orderType == "3") && (
              <div className="row d-none">
                <div className="col-12 ">
                  <div className="col-12 p-0 d-flex align-items-center">
                    <div className="col-xl-4 col-sm-6 p-0">
                      <p style={{ margin: "0" }}>
                      ZAHLUNGSMETHODE FÜR ABHOLUNG

                        <span style={{ color: "#1A83C1" }}>
                          {" "}
                          <sup>*</sup>{" "}
                        </span>
                      </p>
                    </div>
                    <div className="col-xl-8 col-sm-6 pl-3 row m-0">
                      <CheckBoxButton
                        type="checkbox"
                        id="male"
                        name="cash"
                        onChange={onCheckBoxHanlder}
                        value="Zonal Pricing"
                        label="Kasse"
                      ></CheckBoxButton>
                      {/* <input />
                      <label style={{ margin: "0px 20px 0px 5px" }}>Cash</label> */}
                      <CheckBoxButton
                        type="checkbox"
                        id="female"
                        onChange={onCheckBoxHanlder}
                        name="creditCard"
                        value="Mileage Pricing"
                        label=" Kreditkarte"
                      ></CheckBoxButton>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {(inputValue.orderType == "2" || inputValue.orderType == "3") && (
              <div className="row">
                <div className="col-12 ">
                  <div className="col-12 p-0 d-flex align-items-center">
                    <div className="col-xl-4 col-sm-6 p-0">
                      <p style={{ margin: "0" }}>
                      LIEFERART ZUR ABHOLUNG
                        <span style={{ color: "#1A83C1" }}>
                          {" "}
                          <sup>*</sup>{" "}
                        </span>
                      </p>
                    </div>
                    <div className="col-xl-8 col-sm-6 p-0 row m-0">
                      <CheckBoxButton
                        type="checkbox"
                        id="male"
                        name="dcash"
                        onChange={onCheckBoxHanlder}
                        value="Zonal Pricing"
                        label="Kasse"
                      ></CheckBoxButton>
                      <CheckBoxButton
                        type="checkbox"
                        id="female"
                        onChange={onCheckBoxHanlder}
                        name="dcreditCard"
                        value="Mileage Pricing"
                        label="Kreditkarte"
                      ></CheckBoxButton>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
         
          <div className="col-12 mt-3">
           
            <div className="row">
           
            
             
            
              <div className="col-12 mt-3 px-5">
                {inputValue.profileImage &&
                // inputValue.bannerImage &&
                inputValue.storeName &&
                inputValue.ownerName &&
                inputValue.ownerEmail &&
                inputValue.streetName &&
                inputValue.areaName &&
                // inputValue.pricingModel &&
                // inputValue.costForTwo &&
                // inputValue.orderType &&
                inputValue.address &&
                // BusinessPhoneNo.valid &&
                inputValue.localityName 
                && inputValue.accsept == "Enabled"
                 && inputValue.autoDispatch == "Enabled" 
                 && inputValue.forcedAccept == "Enabled" 
                 && inputValue.driverType? (
                  <div className="btn-group btn-group-lg w-100 mt-4 registerEnabled">
                    <button
                      type="button"
                      onClick={() => props.submitStoreData(inputValue)}
                      className="btn btn-primary w-100 nextBtnCustom"
                    >
                      Nächster
                      {/* Register Store */}
                    </button>
                  </div>
                ) : (
                  <div className="col-12 btn-group btn-group-lg w-100">
                    <button
                      className="btn  w-100"
                      type="button"
                      style={{
                        background: "#aaa !important",
                        backgroundColor: "#aaa !important",
                        borderColor: "#aaa !important",
                        cursor: "not-allowed",
                        fontSize: "16px",
                      }}
                    >
                      Nächster
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .store-details .corrency {
          color: ${WHITE_BASE_COLOR};
          background-color: #1A83C1;
        }
        .store-details .after {
          color: ${WHITE_BASE_COLOR};
          background-color: #1A83C1;
        }

        .store-details .minutes {
          color: ${WHITE_BASE_COLOR};
          background-color: #1A83C1;
        }
      `}</style>
    </Wrapper>
  );
};

export default StoreDetailInPuts;
