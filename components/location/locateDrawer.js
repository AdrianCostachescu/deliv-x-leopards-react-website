import React from "react";
// import Drawer from 'material-ui/Drawer'
import Drawer from "@material-ui/core/Drawer";
import MenuItem from "material-ui/MenuItem";
import RaisedButton from "material-ui/RaisedButton";
import { withStyles } from "@material-ui/core/styles";
import LocationSearchInput from "./map";
import LinearProgressBar from "../ui/linearProgress";
import { getCookie } from "../../lib/session";
import { getAddress } from "../../services/address";
import LanguageContext from "../../context/languageContext";
// css
import "../../assets/locationDrawer.scss";

const styles = {
  paper: {
    width: 450
  },
  mobilePaper: {
    width: "100%"
  }
};

export class LocationDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false, addressList: [] };
  }

  handleToggle = () => {
    if (
      (this.props.userProfile &&
        this.props.userProfile.mobile &&
        this.props.userProfile.mobile.length > 1) ||
      getCookie("authorized")
    ) {
      this.getAddress();
    }
    this.setState({ open: !this.state.open });
  };
  handleClose = () => {
    this.setState({ open: false });
    this.props.handleClose();
  };

  getAddress = () => {
    getAddress().then(data => {
      this.setState({ addressList: data.data.data });
    });
  };

  clearAddress = () => {
    this.LocSearchRef.clearAddress();
  };

  componentDidMount() {
    // passing ref to parent to call children functions
    this.props.onRef(this);
  }

  render() {
    const userLocation =
      this.props.userLocation && this.props.userLocation.length > 1
        ? this.props.userLocation
        : null;
    const { classes } = this.props;

    return (
      <>
      <LanguageContext.Consumer>
        {({ lang }) => {
          return (
            <Drawer
              // onRequestChange={(open) => this.setState({ open })}
              open={this.state.open}
              // openSecondary={true}
              // disableSwipeToOpen={true}
              width={this.props.width}
              // docked={true}
              // containerStyle={{ width: this.props.width, overflow: 'hidden' }}
              // containerClassName="scroller horizontal-scroll deliveryLocationSlider"
              anchor="right"
              onClose={this.handleClose}
              classes={{
                paper:
                  this.props.width == "100%"
                    ? classes.mobilePaper
                    : classes.paper
              }}
            >
           <div className ="mai_content">
             <div className="col-12">
               <div className="row justify-content-center">
                 <div className="cross_icon">
                   <i className="fa fa-times" style={{fontSize:"28px",color:"white"}} onClick={this.handleClose}></i>
                 </div>
                 <div className="footer_links">
                 <ul>
     
     <li><a href="/about">Impressum</a></li>
   
     <li><a href="/privacy">Datenschutz</a></li>
     <li><a href="/terms">AGB</a></li>
     <li><a href="/faq">FAQ</a></li>
 </ul>
                 </div>
               </div>
             </div>
           </div>
               
            </Drawer>
          );
        }}
      </LanguageContext.Consumer>
      <style jsx>
        {`
        .footer_links{
          padding-top: 100px;
        }
        ol, ul {
          list-style: none;
          margin: 0;
          padding: 0;
       
      }
      li a{
        text-decoration: none;
        color: white;
        font-size: 22px;
        font-family: Arial,Helvetica,sans-serif;
        font-weight: 600;
      }
      li{
        padding-left: 2rem;
        display: flex;
        padding-right: 2rem;
        justify-content: center;
        padding-top: 40px;
      }
        .cross_icon{
          display: flex;
          justify-content: flex-end;
          width: 100%;
        }
        .mai_content{
          width: 100%;
    height: 100vh;
    background: rgb(26, 131, 193);
    padding:20px;
        }
        `}
      </style>
      </>

    );
  }
}

export default withStyles(styles)(LocationDrawer);
