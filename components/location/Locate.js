import React from "react";
import Slider from "react-slick";
import Footer from "../footer/LandingFooter";
import MFooter from "../footer/Footer";

import Wrapper from "../../hoc/wrapperHoc";
import LocationSearchInput from "./map";
import Spinner from "../ui/loaders/threeDotSpinner";
import LocationDrawer from "./locateDrawer";
import CircularProgressLoader from "../ui/loaders/circularLoader";
import * as enVariables from "../../lib/envariables";
import LanguageSelector from "../language/selector";
import { selectLocale } from "../../actions/language";
import { setCookie, getCookie } from "../../lib/session";
import { DEFAULT_LANG } from "../../lib/envariables";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import { connect } from "react-redux";
import HamBurger from "./hamburger"
class LocationSelect extends React.Component {
  state = {
    loginSliderWidth: "100%",
    language: getCookie("lang") || DEFAULT_LANG,
  };

  showLocationHandler = () => {
    this.setState({ loginSliderWidth: "100%" });
    this.child.handleToggle();
  };

  handleClose = () => {
    this.setState({ loginSliderWidth: "0%" });
  };
  handleClick =(data)=>{
    this.setState({address : data})
    geocodeByAddress(data)
            .then(results => getLatLng(results[0]))
            .then(latLng => this.updateData(latLng))
            .catch(error => console.error('Error', error))
   console.log("tushar",data)
  }
  updateData(data) {
    data['address'] = this.state.address
    this.props.updateLocation(data)
}
  selectLanguage = (event) => {
    this.props.changeLang(event.target.value);
    this.setState({ language: event.target.value });
    setCookie("lang", event.target.value);
  };
  render() {
    let settings = {
      arrows: false,
      autoplay: true,
      dots: false,
      infinite: true,
      vertical: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    let mobileSettings = {
      arrows: false,
      autoplay: true,
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    const locale = this.props.locale;

    return (
      <Wrapper>
          <>
          <main className="mobile-hide">
          <div className="homepage-header" style={{backgroundColor: "#1A83C1"}}>
            <div className="header_module">
            <div className="Join_As_store">
                <p> Jetzt als Fahrer bewerben!</p>
                {/* <button className="button_cpntainer">Apply</button> */}
                <a href={"/storeSignup"} className="button_cpntainer">Bewerben</a>
            </div>
        </div>
            <div  className="d-flex">
                <div  className="homepage-header__content">
                    <a   href="/" aria-current="page" className="nuxt-link-exact-active nuxt-link-active" title="Flink | Groceries delivered in 10 minutes">
                        <img   alt="Flink Logo" src="/static/images/new_imgs/Mask Group 62263.png" className="homepage-header__logo"/>
                        </a>
                         <h1   className="homepage-header__headline">Einkaufen per App, Lieferung in nur 15 Minuten!</h1>
                          <p className="homepage-header__subheadline">Lebensmittel, Snacks und Getränke - wir liefern deine Bestellung sofort an die Haustür! Bestell bequem per App, egal ob frisches Obst oder Gemüse!</p>
                           <div   id="addressCheck" className="homepage-header__form" style={{paddingTop: "40px"}}>
                               <a   href="#" title="Get it on Google Play" target="_blank" rel="noreferrer">
                                   <img   alt="Get it on Google Play" src="/static/images/loopz/language-english.png" width= "250px"/>
                               </a>
                               <a   href="#" title="Download on the App Store" target="_blank" rel="noreferrer" style={{paddingLeft:"15px"}}>
                                   <img   alt="Download on the App Store" src="/static/images/loopz/property-1-english.png" width= "250px"/>
                                   </a>
                                   </div>
                                   </div>
                                    <img   alt="Flink" src="/static/images/loopz/iPhone 12 Pro Free Mockup.png" className="homepage-header__image" />
                                    </div>
                                
        </div>
        <Footer/>
            </main>
    <main className="mobile-show" style={{    backgroundColor: "rgb(26, 131, 193)"}}>
      <div className="col-12 loginMobilePage p-0">
      <HamBurger/>
        <div className="main_container">
        <div className="homepage-header" style={{backgroundColor: "#1A83C1"}}>
            <div className="header_module-mobile">
            <div className="Join_As_store-mobile">
                <p> Jetzt als Fahrer bewerben!</p>
                {/* <button className="button_cpntainer">Apply</button> */}
                <a href={"/storeSignup"} className="button_cpntainer">Bewerben</a>
            </div>
        </div>
            <div  className="">
                <div  className="homepage-header__content-mobile">
                    <a   href="/" aria-current="page" className="nuxt-link-exact-active nuxt-link-active" title="Flink | Groceries delivered in 10 minutes">
                        <img   alt="Flink Logo" src="/static/images/new_imgs/Mask Group 62263.png" className="homepage-header__logo" style={{display:"flex",justifyContent:"center"}}/>
                        </a>
                         <h1   className="homepage-header__headline-mobile">Einkaufen per App, Lieferung in nur 15 Minuten!</h1>
                          <p className="homepage-header__subheadline-mobile">Lebensmittel, Snacks und Getränke - wir liefern deine Bestellung sofort an die Haustür! Bestell bequem per App, egal ob frisches Obst oder Gemüse!</p>
                           <div   id="addressCheck" className="homepage-header__form" style={{paddingTop: "40px",
    width: "100%",
    display: "flex",
    justifyContent: "center"}}>
                               <a   href="#" title="Get it on Google Play" target="_blank" rel="noreferrer">
                                   <img   alt="Get it on Google Play" src="/static/images/loopz/language-english.png" width= "130px"/>
                               </a>
                               <a   href="#" title="Download on the App Store" target="_blank" rel="noreferrer" style={{paddingLeft:"15px"}}>
                                   <img   alt="Download on the App Store" src="/static/images/loopz/property-1-english.png" width= "130px"/>
                                   </a>
                                   </div>
                                   </div>
                                   <div className="image_conatiner">
                                   <img   alt="Flink" src="/static/images/loopz/iPhone 12 Pro Free Mockup.png" className="homepage-header__image-mobile" />
                                   </div>
                                  
                                    </div>
                                
        </div>
        </div>
       
      </div>
      <MFooter/>
    </main>
    <LocationDrawer
              onRef={(ref) => (this.child = ref)}
              width={this.state.loginSliderWidth}
              updateLocation={this.props.updateLocation}
              loading={this.props.loading}
              handleClose={this.handleClose}
              locErrMessage={this.props.locErrMessage}
              getGeoLocation={this.props.getGeoLocation}
            />
        </>
        <style jsx>
          {`
          .homepage-header__image-mobile{
            width: 64%;
            padding-top: 33px;
           
            right: 0;
          }
          .image_conatiner{
            width:100%;
            display: flex;
            justify-content: center;
          }
          .main_container{

          }
          .homepage-header__subheadline-mobile{
            font-size: 20px;
    color: white;
    padding-top: 20px;
    font-weight: 400;
          }
          .header_module-mobile{
            color: rgba(255,255,255,1);
    background-color: #2DC34D;
    margin: 10px 12px;
    border-radius: 30px;
          }
          .homepage-header__content-mobile{
            padding: 19px;
    width: 100%;
          }
         
          .homepage-header__image{
            width:51%;
            padding-top: 60px;
            
            right: 0;
          }
          .homepage-header__subheadline{
            font-size: 20px;
    color: white;
    padding-top: 20px;
    font-weight:500;
          }
          .Join_As_store p{
            color: white;
    font-size: 18px;
    font-weight: 500;
          }
          .Join_As_store-mobile p{
            color: white;
    font-size: 15px;
    font-weight: 400;
          }
          .homepage-header {
            overflow: hidden;
            --tw-bg-opacity: 1;
            background-color: rgba(227, 28, 121, var(--tw-bg-opacity));
        }
        
         .container {
            padding-top: 50px;
        }
        .homepage-header__headline{
            font-size: 3.75rem;
            line-height: 4rem;
            color: white;
            font-family: Arial, Helvetica, sans-serif;
            font-weight:700;
        }
        .homepage-header__headline-mobile{
          font-size: 2.75rem;
          line-height: 3rem;
          color: white;
          font-family: Arial, Helvetica, sans-serif;
          font-weight:700;
        }
        .homepage-header__logo{
          margin-bottom:30px;
          width: 180px;
        }
        .homepage-header__content{
            width: 55%;
            padding-left: 80px;
            padding-top: 60px;
            padding-bottom: 60px;
            display: flex;
    flex-direction: column;
    justify-content: center;
        }
        .header_module{
          color: rgba(255,255,255,1);
          background-color: #2DC34D;
          margin: 10px 73px;
          border-radius: 30px;
        }
        .Join_As_store{
            display: flex;
            justify-content: space-between;
            padding: 13px 40px;
            align-items: center;
        }
        .Join_As_store-mobile{
          display: flex;
          justify-content: space-between;
          padding: 12px;
          align-items: center;
          width: 100%;
      }
        .button_cpntainer{
          border: none;
          background: white;
          width: 120px;
          height: 40px;
          border-radius: 18px;
          font-size: 16px;
          font-weight: 500;
          color: black;
          display: flex;
          justify-content: center;
          align-items: center
        }
        a:hover {
         
          text-decoration: none;
      }
          `}
        </style>
      </Wrapper>
    );
  }
}

const dispatchAction = (dispatch) => {
  return {
    changeLang: (lang) => dispatch(selectLocale(lang)),
  };
};
const stateToProps = (state) => {
  return {
    lang: state.locale,
  };
};
export default connect(stateToProps, dispatchAction)(LocationSelect);
