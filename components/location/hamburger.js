import React,{Component} from "react";
import LocationDrawer from "./locateDrawer";

class Hamburger extends Component{
    state = {
        loginSliderWidth: "100%",
       
      };
    showLocationHandler = () => {
        this.setState({ loginSliderWidth: "100%" });
        this.child.handleToggle();
      };
    
      handleClose = () => {
        this.setState({ loginSliderWidth: "0%" });
      };
    render(){
        return (
            <>
            <div className="row mobileLandingDiv m-0" >
            <i className="fa fa-bars" onClick={this.showLocationHandler} style={{fontSize:"28px",color:"black"}}></i>
            </div>
            <style jsx>
                {`
                 .mobileLandingDiv{
                    display: flex;
            justify-content: flex-end;
            padding: 15px;
            width: 100%;
            background: white;
                  }
                `}
            </style>
            <LocationDrawer
              onRef={(ref) => (this.child = ref)}
              width={this.state.loginSliderWidth}
            
            
              handleClose={this.handleClose}
            
            />
            </>
        )
    }
}
export default Hamburger;