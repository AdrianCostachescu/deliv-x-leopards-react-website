export const EmptyScreen = () => {
  return (
    <div className="my-5">
      <p className="text-center">No stores found nearby</p>
    </div>
  );
};
