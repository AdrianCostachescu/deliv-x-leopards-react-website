import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import { BASE_COLOR } from '../../../lib/envariables';

const CircularProgressLoader = () => (
    <>
    <div className="loaderParent">
        <div className="loader-container">
            <CircularProgress color={BASE_COLOR} size={60} thickness={5} />
        </div>
    </div>
    <style jsx>
        {`
        .loaderParent {
            position: fixed;
            top: 0px;
            left: 0px;
            z-index: 2000;
            background: rgba(0, 0, 0, 0.5);
            width: 100%;
            height: 100vh;
            overflow: hidden !important;
          }
          .loader-container {
            position: relative;
            width: 100%;
            text-align: center;
            padding-top: 50vh;
          }
        `}
    </style>
    </>
);

export default CircularProgressLoader;