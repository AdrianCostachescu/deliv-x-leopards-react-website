import React from "react";


const BackButton = (props) =>{
  return (
      <>
      <div className ="back_button">
          <div className="content_wrap pt-4">
              <a href="/" className="back_style"> {"< zurück"}</a>
          </div>
        {props.name &&<div className="title_wraper pt-4">
              <h1>{props.name}</h1>
          </div>}
      </div>
      <style jsx>
          {`
          .back_style{
            color: black;
            font-size: 18px;
            font-weight: 500;
          }
          .title_wraper h1{
            font-weight: bold;
            font-size: 52px;
          }
          .back_style:hover{
              text-decoration:none;
          }
          `}
      </style>
      </>
  )
}
export default BackButton;