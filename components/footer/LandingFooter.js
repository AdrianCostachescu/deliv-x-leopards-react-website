import React from "react";
import * as enVariables from "../../lib/envariables";
import { getCookie } from "../../lib/session";
const Footer = props => {
  const { locale } = props;
  const lang = props.selectedLang || "en";
    console.log(lang);
  return (
    <>
    <div className="footer_com">
    <div className="background">
        <div className="footer_links">
           <div className="middle_con">
               <ul>
     
     <li><a href="/about">Impressum</a></li>
   
     <li><a href="/privacy">Datenschutz</a></li>
     <li><a href="/terms">AGB</a></li>
     <li><a href="/faq">FAQ</a></li>
 </ul>
           </div>
           <div className="footer-text">
               <p className="">© 2021 Leopards GmbH</p>
           </div>
        </div>
    </div>
</div>
<style jsx>
  {`
  .background{
    background: #1A83C1;
    height: 160px;
    padding-top: 40px;
}

ol, ul {
    list-style: none;
    margin: 0;
    padding: 0;
    display: flex;
}
li a{
    text-decoration: none;
    color: white;
    font-size: 18px;
    font-family: Arial, Helvetica, sans-serif;
}
li{
    padding-left: 2rem;
    padding-right: 2rem;
}
.footer_links{
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
   
}
.footer-text{
    margin-top: 40px;
}
.footer-text p{
  font-size: 18px;
    color: white;
    font-weight: 500;
}
  `}
</style>
</>
  );
};
export default Footer;
