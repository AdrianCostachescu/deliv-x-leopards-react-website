import React from "react";
import * as enVariables from "../../lib/envariables";
import { getCookie } from "../../lib/session";
import {connect} from "react-redux"
const Footer = props => {
  const { locale } = props;
  const lang = props.selectedLang || "en";
    console.log(lang);
  return (
    <>
    <footer id="footer" className="">
     
     <div className="footer-text">
               <p className="">© 2021 Leopards GmbH</p>
           </div>
    </footer>
    <style jsx>
      {`
      .footer-text{
        margin-top: 40px;
    }
    .footer-text p{
      font-size: 18px;
    color: white;
    text-align: center;
    padding-bottom: 40px;
    font-weight: 500;
    margin: 0;
    }
    #footer{
      background: rgb(26, 131, 193);
      padding-top: 16px;
    }
      `}
    </style>
    </>
  );
};
// export default Footer;

const mapStateToProps = state => {
    return {
        reduxState: state,
        stores: state.stores,
        myCart: state.cartList,
        appConfig: state.appConfig,
        userProfile: state.userProfile,
        locale:state.locale,
        selectedLang: state.selectedLang
    };
};

export default connect(mapStateToProps)(Footer)
