import { delay } from 'redux-saga'
import { all, call, put, take, takeLatest } from 'redux-saga/effects'
import axios from "axios";

import * as actions from "../actions/index"
import { setCookie, getCookie, getCookiees, removeCookie } from '../lib/session'
import { addToCartService, getCart, editCart, deleteCart, cartNewWithAddOns } from '../services/cart'
import { isProductAvailable } from '../lib/cart/checkAvailability';

export function* addToCartSaga(action) {
    yield put(actions.loadingStart())
    const cartResponse = yield addToCartService(action.data);
    yield console.log("cartResponse", cartResponse);
    
    yield cartResponse.error ?
        cartResponse.status == 498 ?
            yield put(actions.expSession())
            : ''
        : yield put(actions.getCart())
        ;
    yield put(actions.loadingStop());
}

export function* repeatCartSaga(action) {
    yield put(actions.loadingStart())
    const cartResponse = yield cartNewWithAddOns(action.data)
    yield put(actions.getCart())
    yield put(actions.loadingStop())
}

export function* getCartSaga(action) {
    const getCartResponse = yield getCart()
    let cartData; let cartProducts = [];

    getCartResponse.error ?
        yield put(actions.updateCart([], []))
        : (
            cartData = yield getCartResponse.data.data,
            cartProducts = [],
            cartData.cart ? yield cartData.cart.map((cart, index) => {
                cart.products ? cart.products.map((product, index) => {
                    cartProducts.push(product)
                }) : ''
            }) : '',
            yield put(actions.updateCart(getCartResponse.data.data, cartProducts))
        )
}

export function* editCartSaga(action) {
    
    const productAvailability = yield isProductAvailable(action.data);
    console.log(productAvailability,"productAvailability")
    // check for available quantity, if not available, return with error.
    if(!productAvailability){
        if(typeof toastr !== "undefined"){
            toastr.error(`Only ${action.data.quantity - 1} quantity is available of this product.`);
        }
        return;
    }
    
    const editcartRes = yield action.data.quantity > 0 ? editCart(action.data) : deleteCart(action.data);
    yield put(actions.getCart())
}

export function* selectDeliveryTypeSaga(action) {
    yield setCookie("deliveryType", action.delivery)
}